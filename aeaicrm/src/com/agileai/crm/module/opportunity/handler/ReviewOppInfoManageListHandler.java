package com.agileai.crm.module.opportunity.handler;

import java.util.List;

import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.cxmodule.OppInfoManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.DispatchRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class ReviewOppInfoManageListHandler
        extends StandardListHandler {
    public ReviewOppInfoManageListHandler() {
        super();
        this.editHandlerClazz = ReviewOppInfoManageEditHandler.class;
        this.serviceId = buildServiceId(OppInfoManage.class);
    }
	public ViewRenderer prepareDisplay(DataParam param){
		User user = (User) this.getUser();
		PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
		if(privilegeHelper.isSalesDirector()){
			setAttribute("doConfirm", true);
		}else if(!privilegeHelper.isSalesDirector()){
			setAttribute("doConfirm", false);
		}
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
    protected void processPageAttributes(DataParam param) {
        setAttribute("OPP_STATE",
                     FormSelectFactory.create("OPP_STATE")
                                      .addSelectedValue(param.get("OPP_STATE")));
    	initMappingItem("OPP_STATE", FormSelectFactory.create("OPP_STATE")
				.getContent());
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "OPP_STATE", "");
        initParamItem(param, "OPP_NAME", "");
        initParamItem(param, "OPP_CONCERN_PRODUCT", "");
        initParamItem(param, "CUST_ID_NAME", "");
        initParamItem(param, "CLUE_SALESMAN_NAME", "");
    }
    @PageAction
    public ViewRenderer doCreateOrderAction(DataParam param) {
		storeParam(param);
		String a = "OppCreateOrderInfoEdit";
		return new DispatchRenderer(getHandlerURL(a) + "&"
				+ OperaType.KEY + "=doCreateOrderAction&comeFrome=doCreateOrderAction");
	}
    @PageAction
	public ViewRenderer doConfirm(DataParam param) {
		String oppId = param.get("OPP_ID");
		getService().changeStateRecord(oppId);
		return prepareDisplay(param);
	}
    @PageAction
	public ViewRenderer doDispose(DataParam param) {
		storeParam(param);
		return new DispatchRenderer(getHandlerURL(this.editHandlerClazz) + "&"
				+ OperaType.KEY
				+ "=doDispose&comeFrome=doDispose");
	}
    protected OppInfoManage getService() {
        return (OppInfoManage) this.lookupService(this.getServiceId());
    }
}
