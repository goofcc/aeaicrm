package com.agileai.crm.service.taskreview;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.crm.cxmodule.ColdCallsManage;
import com.agileai.crm.cxmodule.FollowUpManage;
import com.agileai.crm.cxmodule.TaskCycle8ContentManage;
import com.agileai.crm.cxmodule.TaskCycleManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.ws.BaseRestService;


public class TaskReviewImpl extends BaseRestService implements TaskReview {
    protected TaskCycleManage taskCycleService() {
        return (TaskCycleManage) this.lookupService(TaskCycleManage.class);
    }
    
    protected ColdCallsManage coldCallsService() {
        return (ColdCallsManage) this.lookupService(ColdCallsManage.class);
    }
    
    protected FollowUpManage followUpService() {
        return (FollowUpManage) this.lookupService(FollowUpManage.class);
    }
    
    protected TaskCycle8ContentManage taskCycle8ContentManageService() {
        return (TaskCycle8ContentManage) this.lookupService(TaskCycle8ContentManage.class);
    }

	@Override
	public String findWorkCheckList() {
		String responseText = null;
		try {
			List<DataRow> rsList = taskCycleService().findRecords(new DataParam());
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				JSONObject jsonObject = new JSONObject();
				DataRow dataRow = rsList.get(i);
				jsonObject.put("tcId", dataRow.get("TC_ID"));
				jsonObject.put("tcBegin", dataRow.get("TC_BEGIN"));
				jsonObject.put("tcEnd", dataRow.get("TC_END"));
				jsonArray.put(jsonObject);
			}
			responseText = jsonArray.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findStrangeVisitList(String tcId, String saleId) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow taskReviewIdRow = taskCycleService().getTaskReviewIdRecord(new DataParam("tcId", tcId, "saleId", saleId));
			if(taskReviewIdRow != null){
				String taskReviewId = taskReviewIdRow.getString("TASK_REVIEW_ID");
				List<DataRow> rsList = coldCallsService().findMasterRecords(new DataParam("TASK_REVIEW_ID", taskReviewId,"orgSalesman" ,saleId));
				JSONArray jsonArray = new JSONArray();
				for(int i=0;i<rsList.size();i++){
					DataRow dataRow = rsList.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("taskId", dataRow.get("TASK_ID"));
					jsonObject1.put("orgId", dataRow.get("ORG_ID"));
					jsonObject1.put("name", dataRow.get("ORG_NAME"));
					String classification = FormSelectFactory.create("ORG_CLASSIFICATION").getText(dataRow.getString("ORG_CLASSIFICATION"));
					jsonObject1.put("classification", classification);
					String state = FormSelectFactory.create("PER_STATE").getText(dataRow.getString("ORG_STATE"));
					jsonObject1.put("state", state);
					jsonObject1.put("updateTime", dataRow.get("ORG_UPDATE_TIME"));
					String labels = FormSelectFactory.create("ORG_LABELS").getText(dataRow.getString("ORG_LABELS"));
					jsonObject1.put("labels", labels);
					String followState = FormSelectFactory.create("TASK_FOLLOW_STATUS").getText(dataRow.getString("TASK_FOLLOW_STATE"));
					jsonObject1.put("followState", followState);
					jsonArray.put(jsonObject1);
				}
				jsonObject.put("datas", jsonArray);
				jsonObject.put("taskReviewId", taskReviewId);
			}else{
				jsonObject.put("datas", "isEmpty");
			}

			responseText = jsonObject.toString();
		} catch (JSONException e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findIntentionFollowUpList(String tcId, String saleId) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow taskReviewIdRow = taskCycleService().getTaskReviewIdRecord(new DataParam("tcId", tcId, "saleId", saleId));
			if(taskReviewIdRow != null){
				String taskReviewId = taskReviewIdRow.getString("TASK_REVIEW_ID");
				List<DataRow> reList = followUpService().findMasterRecords(new DataParam("TASK_REVIEW_ID", taskReviewId,"userId", saleId));
				JSONArray jsonArray = new JSONArray();
				for(int i=0;i<reList.size();i++){
					DataRow dataRow = reList.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("taskId", dataRow.get("TASK_ID"));
					jsonObject1.put("custId", dataRow.get("CUST_ID"));
					jsonObject1.put("custName", dataRow.get("CUST_NAME"));
					String custIndustry = FormSelectFactory.create("CUST_INDUSTRY").getText(dataRow.getString("CUST_INDUSTRY"));
					jsonObject1.put("custIndustry", custIndustry);
					String custScale = FormSelectFactory.create("CUST_SCALE").getText(dataRow.getString("CUST_SCALE"));
					jsonObject1.put("custScale", custScale);
					String custNature = FormSelectFactory.create("CUST_NATURE").getText(dataRow.getString("CUST_NATURE"));
					jsonObject1.put("custNature", custNature);
					String custState = FormSelectFactory.create("CUST_STATE").getText(dataRow.getString("CUST_STATE"));
					jsonObject1.put("custState", custState);
					String followState = FormSelectFactory.create("TASK_FOLLOW_STATUS").getText(dataRow.getString("TASK_FOLLOW_STATE"));
					jsonObject1.put("followState", followState);
					jsonArray.put(jsonObject1);
				}
				jsonObject.put("datas", jsonArray);
				jsonObject.put("taskReviewId", taskReviewId);
			}else{
				jsonObject.put("datas", "isEmpty");
			}

			responseText = jsonObject.toString();
		} catch (JSONException e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String getSummaryInfo(String tcId, String saleId) {
		String rspText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow taskReviewIdRow = taskCycleService().getTaskReviewIdRecord(new DataParam("tcId", tcId, "saleId", saleId));
			if(taskReviewIdRow != null){
				String taskReviewId = taskReviewIdRow.getString("TASK_REVIEW_ID");
				DataParam param = new DataParam();
				
				param.put("TASK_REVIEW_ID", taskReviewId);
				DataRow record = taskCycle8ContentManageService().getSummaryRecord(param);
				
				String state = FormSelectFactory.create("TASK_REVIEW_STATE").getText(record.getString("TASK_REVIEW_STATE"));
				jsonObject.put("taskReviewState", state);
				jsonObject.put("taskReviewVisitsTotal", record.get("TASK_REVIEW_VISITS_TOTAL"));
				jsonObject.put("taskReviewStrange", record.get("TASK_REVIEW_STRANGE"));
				jsonObject.put("taskReviewDesc", record.get("TASK_REVIEW_DESC"));
				jsonObject.put("taskReviewStrangeTask", record.get("TASK_REVIEW_STRANGE_TASK"));
				jsonObject.put("taskReviewStrangeFollow", record.get("TASK_REVIEW_STRANGE_FOLLOW"));
				jsonObject.put("taskReviewStrangeVisit", record.get("TASK_REVIEW_STRANGE_VISIT"));
				jsonObject.put("taskReviewIntentionTask", record.get("TASK_REVIEW_INTENTION_TASK"));
				jsonObject.put("taskReviewIntentionFollow", record.get("TASK_REVIEW_INTENTION_FOLLOW"));
				jsonObject.put("taskReviewIntentionVisit", record.get("TASK_REVIEW_INTENTION_VISIT"));
				
				param.put("SALE_ID",record.get("SALE_ID"),"TC_ID",record.get("TC_ID"));
				DataRow proNumRow = taskCycle8ContentManageService().getProNumRecord(param);
				long proNum = (Long) proNumRow.get("PRO_NUM");
				jsonObject.put("proNum", proNum);
				
				param.put("SALE_ID",record.get("SALE_ID"),"TC_ID",record.get("TC_ID"));
				DataRow custNumRow = taskCycle8ContentManageService().getCustNumRecord(param);
				long custNum = (Long) custNumRow.get("CUST_NUM");
				jsonObject.put("custNum", custNum);
				
				List<DataRow> oppRecords = taskCycle8ContentManageService().queryOppRecords(new DataParam("TC_ID", record.get("TC_ID"), "SALE_ID", saleId));
				jsonObject.put("oppNum", oppRecords.size());
				JSONArray oppRecordsArray = new JSONArray();
				for(int i=0;i<oppRecords.size();i++){
					DataRow dataRow = oppRecords.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("oppName", dataRow.get("OPP_NAME"));
					jsonObject1.put("custIdName", dataRow.get("CUST_ID_NAME"));
					jsonObject1.put("contIdName", dataRow.get("CONT_ID_NAME"));
					jsonObject1.put("oppConcernProduct", dataRow.get("OPP_CONCERN_PRODUCT"));
					jsonObject1.put("clueSalesmanName", dataRow.get("CLUE_SALESMAN_NAME"));
					jsonObject1.put("oppCreaterName", dataRow.get("OPP_CREATER_NAME"));
					jsonObject1.put("oppCreateTime", dataRow.get("OPP_CREATE_TIME"));
					oppRecordsArray.put(jsonObject1);
				}
				jsonObject.put("oppRecords", oppRecordsArray);
				
				List<DataRow> orderRecords = taskCycle8ContentManageService().queryOrderRecords(new DataParam("TC_ID", record.get("TC_ID"), "SALE_ID", saleId));
				jsonObject.put("orderNum", orderRecords.size());
				JSONArray orderRecordsArray = new JSONArray();
				for(int i=0;i<orderRecords.size();i++){
					DataRow dataRow = orderRecords.get(i);
					JSONObject jsonObject1 = new JSONObject();
					jsonObject1.put("oppIdName", dataRow.get("OPP_ID_NAME"));
					jsonObject1.put("orderName", dataRow.get("ORDER_NAME"));
					jsonObject1.put("orderChief", dataRow.get("ORDER_CHIEF"));
					jsonObject1.put("orderCost", dataRow.get("ORDER_COST"));
					jsonObject1.put("clueSalesmanName", dataRow.get("CLUE_SALESMAN_NAME"));
					jsonObject1.put("orderCreaterName", dataRow.get("ORDER_CREATER_NAME"));
					jsonObject1.put("orderCreateTime", dataRow.get("ORDER_CREATE_TIME"));
					orderRecordsArray.put(jsonObject1);
				}
				jsonObject.put("orderRecords", oppRecordsArray);
				jsonObject.put("taskReviewId", taskReviewId);
			}else{
				jsonObject.put("datas", "isEmpty");
			}
			
			rspText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		
		return rspText;
	}

	@Override
	public String updateSummaryRecord(String taskReviewId, String taskReviewState) {
		String rspText = "fail";
		taskCycle8ContentManageService().updateSummaryRecord(new DataParam("TASK_REVIEW_ID", taskReviewId, "TASK_REVIEW_STATE", taskReviewState));
		rspText = "success";
		return rspText;
	}
}
