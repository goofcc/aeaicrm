package com.agileai.crm.service.customer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.common.PrivilegeHelper;
import com.agileai.crm.cxmodule.ContListSelect;
import com.agileai.crm.cxmodule.CustomerGroup8ContentManage;
import com.agileai.crm.cxmodule.OppInfoManage;
import com.agileai.crm.cxmodule.OrderInfoManage;
import com.agileai.crm.cxmodule.SalerListSelect;
import com.agileai.crm.cxmodule.VisitManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.ws.BaseRestService;
import com.agileai.util.DateUtil;
import com.agileai.util.StringUtil;


public class CustomersImpl extends BaseRestService implements Customers {
	
	@Override
	public String findMyCustomersList(String searchWord) {
		String responseText = null;
		DataParam param = new DataParam();
		try {
			if(StringUtil.isNotNullNotEmpty(searchWord)){
	    		if(!"".equals(searchWord)){
	    			JSONObject searchObject = new JSONObject(searchWord);
	    			String level = "";
	    			String progressState = "";
	    			String saleMan = "";
	    			
	    			if(searchObject.has("level")){
	    				level = (String) searchObject.get("level");
	    			}
	    			if(searchObject.has("progressState")){
	    				progressState = (String) searchObject.get("progressState");
	    			}
	    			if(searchObject.has("saleMan")){
	    				saleMan = (String) searchObject.get("saleMan");
	    			}
	    			
		        	param.put("level",level);
		        	param.put("progressState",progressState);
		        	param.put("saleMan", saleMan);
	    		}
			}
        	
			JSONObject jsonObject = new JSONObject();
			User user = (User) getUser();
			PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
			if (!privilegeHelper.isSalesDirector()) {
				param.put("userId", user.getUserId());
			} else {
				param.put("userId", "");
			}
			
			List<DataRow> reList = getService().findMyCustomersList(param);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow.get("CUST_ID"));
				jsonObject1.put("name", dataRow.get("CUST_NAME"));
				jsonObject1.put("progressState", dataRow.get("CUST_PROGRESS_STATE"));
				jsonObject1.put("level", dataRow.get("CUST_LEVEL"));
				jsonObject1.put("createName", dataRow.get("CUST_CREATE_NAME"));
				DataParam salesParam = new DataParam();
				salesParam.put("custId", dataRow.get("CUST_ID"));
				List<DataRow> salesList = getService().findSalesList(salesParam);
				String saleNames = "";
				for(int j=0;j<salesList.size();j++){
					DataRow saleRow = salesList.get(j);
					saleNames = saleNames + saleRow.get("USER_NAME") + "|";
				}
				if("".equals(saleNames)){
					saleNames = "暂无销售";
				}
				jsonObject1.put("saleNames",saleNames);
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String findMyCustForNameList(String searchWord) {
		String responseText = null;
		DataParam param = new DataParam();
		try {
			param.put("searchWord", searchWord);
			JSONObject jsonObject = new JSONObject();
			User user = (User) getUser();
			PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
			if (!privilegeHelper.isSalesDirector()) {
				param.put("userId", user.getUserId());
			} else {
				param.put("userId", "");
			}
			
			List<DataRow> reList = getService().findMyCustomersList(param);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow.get("CUST_ID"));
				jsonObject1.put("name", dataRow.get("CUST_NAME"));
				jsonObject1.put("progressState", dataRow.get("CUST_PROGRESS_STATE"));
				jsonObject1.put("level", dataRow.get("CUST_LEVEL"));
				jsonObject1.put("createName", dataRow.get("CUST_CREATE_NAME"));
				DataParam salesParam = new DataParam();
				salesParam.put("custId", dataRow.get("CUST_ID"));
				List<DataRow> salesList = getService().findSalesList(salesParam);
				String saleNames = "";
				for(int j=0;j<salesList.size();j++){
					DataRow saleRow = salesList.get(j);
					saleNames = saleNames + saleRow.get("USER_NAME") + "|";
				}
				if("".equals(saleNames)){
					saleNames = "暂无销售";
				}
				jsonObject1.put("saleNames",saleNames);
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String getCustomersDetails(String id) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = getService().getCustomerInfoRecord(new DataParam("CUST_ID",id));
			jsonObject.put("id", dataRow.get("CUST_ID"));
			jsonObject.put("name", dataRow.get("CUST_NAME"));
			jsonObject.put("level", dataRow.get("CUST_LEVEL"));
			jsonObject.put("progressState", dataRow.get("CUST_PROGRESS_STATE"));
			jsonObject.put("industry", dataRow.get("CUST_INDUSTRY"));
			jsonObject.put("address", dataRow.get("CUST_ADDRESS"));
			jsonObject.put("province", dataRow.get("CUST_PROVINCE"));
			jsonObject.put("web", dataRow.get("CUST_COMPANY_WEB"));
			jsonObject.put("introduce", dataRow.get("CUST_INTRODUCE"));
			String custState =FormSelectFactory.create("CUST_STATE").getText(dataRow.getString("CUST_STATE"));
			jsonObject.put("custState", custState);
			
			if(dataRow.getString("CUST_STATE").equals("Submit") || dataRow.getString("CUST_STATE").equals("Confirm")){
				jsonObject.put("recordState", false);
			}else{
				jsonObject.put("recordState", true);
			}
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String createCustomers(String info) {
    	String responseText = "fail";
    	try {
        	JSONObject jsonObject = new JSONObject(info);
        	User user = (User) this.getUser();
        	String datetime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
        	String userId = user.getUserId();
        	
        	String custId = KeyGenerator.instance().genKey();
        	String name = jsonObject.get("name").toString();
        	String level = jsonObject.get("level").toString();
        	String progressState = jsonObject.get("progressState").toString();
        	String industry = jsonObject.get("industry").toString();
        	String address = jsonObject.get("address").toString();
        	String province = jsonObject.get("province").toString();
        	String web = jsonObject.get("web").toString();
        	String introduce = jsonObject.get("introduce").toString();
        	
        	DataParam createParam = new DataParam("CUST_ID",custId,"CUST_NAME",name,"CUST_LEVEL",level,"CUST_PROGRESS_STATE",progressState,"CUST_INDUSTRY",industry,"CUST_ADDRESS",address,"CUST_PROVINCE",province,"CUST_COMPANY_WEB",web,"CUST_INTRODUCE",introduce,"CUST_CREATE_ID",userId,"CUST_CREATE_TIME",datetime,"CUST_STATE","init");
        	getService().createCustomers(createParam);
        	
        	DataRow dataRow = getService().queryTempGrpId(createParam);
        	String grpId = dataRow.getString("GRP_ID");
        	DataParam createCustGrpRelParam = new DataParam("GRP_ID",grpId,"CUST_ID",custId);
        	getService().insertCustomerInfoRelation(createCustGrpRelParam);
    		DataParam salesManParam = new DataParam("custId",custId,"userId",user.getUserId());
    		getService().addSalesManRelation(salesManParam);
        	
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }

	@Override
	public String editCustomersDetails(String info) {
    	String responseText = "fail";
    	try {
        	JSONObject jsonObject = new JSONObject(info);
        	User user = (User) this.getUser();
        	String datetime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
        	String userId = user.getUserId();
        	
        	String id = jsonObject.get("id").toString();
        	String name = jsonObject.get("name").toString();
        	String level = jsonObject.get("level").toString();
        	String progressState = jsonObject.get("progressState").toString();
        	String industry = jsonObject.get("industry").toString();
        	String address = jsonObject.get("address").toString();
        	String province = jsonObject.get("province").toString();
        	String web = jsonObject.get("web").toString();
        	String introduce = jsonObject.get("introduce").toString();
        	
        	DataParam createParam = new DataParam("CUST_ID",id,"CUST_NAME",name,"CUST_LEVEL",level,"CUST_PROGRESS_STATE",progressState,"CUST_INDUSTRY",industry,"CUST_ADDRESS",address,"CUST_PROVINCE",province,"CUST_COMPANY_WEB",web,"CUST_INTRODUCE",introduce,"CUST_CREATE_ID",userId,"CUST_CREATE_TIME",datetime,"CUST_STATE","init");
        	getService().editCustomersDetails(createParam);
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
	}
	
	@Override
	public String deleteCustomersInfo(String id) {
    	String responseText = "fail";
    	try {
    		DataParam param = new DataParam("CUST_ID",id);
        	getService().deletContentsInfo(param);
    		getService().deleteCustomersInfo(param);
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }

	@Override
	public String getCustomersInfo(String id) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = getService().getCustomersInfo(new DataParam("CUST_ID",id));
			jsonObject.put("id", dataRow.get("CUST_ID"));
			jsonObject.put("name", dataRow.get("CUST_NAME"));
			jsonObject.put("progressState", dataRow.get("CUST_PROGRESS_STATE"));
			jsonObject.put("level", dataRow.get("CUST_LEVEL"));
			jsonObject.put("createName", dataRow.get("CUST_CREATE_NAME"));			
			DataParam salesParam = new DataParam();
			salesParam.put("custId", dataRow.get("CUST_ID"));
			List<DataRow> salesList = getService().findSalesList(salesParam);
			String saleNames = "";
			for(int j=0;j<salesList.size();j++){
				DataRow saleRow = salesList.get(j);
				saleNames = saleNames + saleRow.get("USER_NAME") + "|";
			}
			if("".equals(saleNames)){
				saleNames = "暂无销售";
			}
			jsonObject.put("saleNames",saleNames);			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String updateCustStateInfo(String id, String state) {
		String responseText = "fail";
		getService().updateStateInfoRecord(new DataParam("CUST_ID",id,"CUST_STATE",state));
		responseText = "success";
		return responseText;
	}

	@Override
	public String findCustomersVisitInfo(String custId) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			List<DataRow> reList = getService().findCustomersVisitInfo(new DataParam("CUST_ID",custId));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow.get("VISIT_ID"));
				jsonObject1.put("name", dataRow.get("VISIT_USER_NAME"));
				jsonObject1.put("date", dataRow.get("VISIT_DATE").toString().substring(0, 10));
				jsonObject1.put("effect", dataRow.get("VISIT_EFFECT"));
				jsonObject1.put("visitType", dataRow.get("VISIT_TYPE"));
				jsonObject1.put("receptionName", dataRow.get("VISIT_RECEPTION_NAME"));
				jsonObject1.put("receptionPhone", dataRow.get("VISIT_RECEPTION_PHONE"));
				String visitStateCode=dataRow.getString("VISIT_STATE");
				String visitState =FormSelectFactory.create("CUST_STATE").getText(visitStateCode);
				jsonObject1.put("visitState", visitState);
				String custState = dataRow.get("CUST_STATE").toString();
				jsonObject1.put("custState", custState);
				
				if(visitState.equals("提交")){
					jsonObject1.put("recordState", false);
				}else{
					jsonObject1.put("recordState", true);
				}
				
				if(custState.equals("Confirm") && visitState.equals("提交")){
					jsonObject1.put("confirmOppState", true);
				}else{
					jsonObject1.put("confirmOppState", false);
				}
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String createCustomersVisitInfo(String info) {
    	String responseText = "fail";
    	try {
        	JSONObject jsonObject = new JSONObject(info);
        	User user = (User) this.getUser();
        	String userId = user.getUserId();
        	
        	String id = jsonObject.get("VISIT_CUST_ID").toString();
        	String type = jsonObject.get("type").toString();
        	String receptionName = jsonObject.get("receptionName").toString();
        	String receptionSex = jsonObject.get("receptionSex").toString();
        	String receptionJob = jsonObject.get("receptionJob").toString();
        	String receptionPhone = jsonObject.get("receptionPhone").toString();
        	
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        	String visitDateUTC = jsonObject.get("visitDate").toString().replace("Z", " UTC");
        	Date date = format.parse(visitDateUTC);
        	String visitDate = DateUtil.format(DateUtil.YYMMDD_HORIZONTAL, date);
        	
        	String effect = jsonObject.get("effect").toString();
        	
        	DataParam createParam = new DataParam
        			("VISIT_CUST_ID",id,"VISIT_TYPE",type,"VISIT_USER_ID",userId,"VISIT_RECEPTION_NAME",receptionName,"VISIT_RECEPTION_SEX",
        					receptionSex,"VISIT_RECEPTION_JOB",receptionJob,"VISIT_RECEPTION_PHONE",
        					receptionPhone,"VISIT_DATE",visitDate,"VISIT_EFFECT",effect,
        					"VISIT_ FILL_ID",userId,"VISIT_FILL_TIME",new Date(),"VISIT_STATE","init");
        	getService().createCustomersVisitInfo(createParam);
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }

	@Override
	public String deleteCustomersVisitInfo(String id) {
    	String responseText = "fail";
    	try {
        	getService().deleteCustomersVisitInfo(new DataParam("VISIT_ID",id));
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }

	@Override
	public String getCustomersVisitInfo(String id) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = getService().getCustomersVisitInfo(new DataParam("VISIT_ID",id));
			jsonObject.put("id", dataRow.get("VISIT_ID"));
			jsonObject.put("custId", dataRow.get("VISIT_CUST_ID"));
			jsonObject.put("type", dataRow.get("VISIT_TYPE"));
			jsonObject.put("receptionName", dataRow.get("VISIT_RECEPTION"));
			jsonObject.put("receptionSex", dataRow.get("VISIT_RECEPTION_SEX"));
			jsonObject.put("receptionJob", dataRow.get("VISIT_RECEPTION_JOB"));
			jsonObject.put("receptionPhone", dataRow.get("VISIT_RECEPTION_PHONE"));
			jsonObject.put("visitDate", dataRow.get("VISIT_DATE"));
			jsonObject.put("effect", dataRow.get("VISIT_EFFECT"));
			String visitStateCode=dataRow.getString("VISIT_STATE");
			String visitState =FormSelectFactory.create("CUST_STATE").getText(visitStateCode);
			jsonObject.put("visitState", visitState);
			
			if(visitState.equals("提交")){
				jsonObject.put("judgeState", false);
			}else{
				jsonObject.put("judgeState", true);
			}
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String editCustomersVisitInfo(String info) {
    	String responseText = "fail";
    	try {
        	JSONObject jsonObject = new JSONObject(info);
        	User user = (User) this.getUser();
        	String userId = user.getUserId();
        	
        	String visitId = jsonObject.get("id").toString();
        	String type = jsonObject.get("type").toString();
        	String receptionName = jsonObject.get("receptionName").toString();
        	String receptionSex = jsonObject.get("receptionSex").toString();
        	String receptionJob = jsonObject.get("receptionJob").toString();
        	String receptionPhone = jsonObject.get("receptionPhone").toString();
        	
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z");
        	String visitDateUTC = jsonObject.get("visitDate").toString().replace("Z", " UTC");
        	Date date = format.parse(visitDateUTC);
        	String visitDate = DateUtil.format(DateUtil.YYMMDD_HORIZONTAL, date);
        	
        	String effect = jsonObject.get("effect").toString();
        	
        	DataParam editParam = new DataParam
        			("VISIT_ID",visitId,"VISIT_TYPE",type,"VISIT_USER_ID",userId,"VISIT_RECEPTION_NAME",receptionName,"VISIT_RECEPTION_SEX",
        					receptionSex,"VISIT_RECEPTION_JOB",receptionJob,"VISIT_RECEPTION_PHONE",
        					receptionPhone,"VISIT_DATE",visitDate,"VISIT_EFFECT",effect,
        					"VISIT_ FILL_ID",userId,"VISIT_FILL_TIME",new Date(),"VISIT_STATE","init");
        	getService().editCustomersVisitInfo(editParam);
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }
	
	@Override
	public String submitVisitStateInfo(String id) {
		String responseText = "fail";
		DataParam param = new DataParam();
		param.put("VISIT_ID", id);
		param.put("VISIT_STATE", "Submit");
		User user = (User) getUser();
		param.put("VISIT_CONFIRM_ID", user.getUserId());
		String confirmTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
		param.put("VISIT_CONFIRM_TIME", confirmTime);
		visitService().updateConfirmStateInfoRecord(param);
		responseText = "success";
		return responseText;
	}

	@Override
	public String findCustomersOppInfo(String custId) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			List<DataRow> reList = getService().findCustomersOppInfo(new DataParam("CUST_ID",custId));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow.get("OPP_ID"));
				jsonObject1.put("saleMan", dataRow.get("SALESMAN"));
				jsonObject1.put("name", dataRow.get("OPP_NAME"));
				jsonObject1.put("datetime", dataRow.get("OPP_CREATE_TIME"));
				jsonObject1.put("level", dataRow.get("OPP_LEVEL"));
				String concernProduct=FormSelectFactory.create("OPP_CONCERN_PRODUCT").getText(dataRow.getString("OPP_CONCERN_PRODUCT"));
				jsonObject1.put("concernProduct", concernProduct);
				String oppStateCode = dataRow.getString("OPP_STATE");
				String oppState=FormSelectFactory.create("OPP_STATE").getText(oppStateCode);
				jsonObject1.put("oppState", oppState);
				
				if(dataRow.getString("OPP_STATE").equals("1")){
					jsonObject1.put("recordState", false);
				}
				else if(dataRow.getString("OPP_STATE").equals("2")){
					jsonObject1.put("submitState", true);
					jsonObject1.put("recordState", false);
				}else{
					jsonObject1.put("submitState", false);
					jsonObject1.put("recordState", true);
				}
				
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String getCustomersOppInfo(String id) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = getService().getCustomersOppInfo(new DataParam("OPP_ID",id));
			
			jsonObject.put("id", dataRow.get("OPP_ID"));
			jsonObject.put("custId", dataRow.get("CUST_ID"));
			jsonObject.put("name", dataRow.get("OPP_NAME"));
			jsonObject.put("concernProduct", dataRow.get("OPP_CONCERN_PRODUCT"));
			jsonObject.put("expectInvest", dataRow.get("OPP_EXPECT_INVEST"));
			jsonObject.put("level", dataRow.get("OPP_LEVEL"));
			jsonObject.put("oppState", dataRow.get("OPP_STATE"));
			
			if(dataRow.get("OPP_STATE").equals("1")){
				jsonObject.put("judgeState", false);
			}else if(dataRow.get("OPP_STATE").equals("2")){
				jsonObject.put("judgeState", false);
			}else{
				jsonObject.put("judgeState", true);
			}
			
			jsonObject.put("des", dataRow.get("OPP_DES"));
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String editCustomersOppInfo(String info) {
    	String responseText = "fail";
    	try {
        	JSONObject jsonObject = new JSONObject(info);
        	User user = (User) this.getUser();
        	String userId = user.getUserId();
        	
        	String id = jsonObject.get("id").toString();
        	String name = jsonObject.get("name").toString();
        	String productNames = jsonObject.get("productNames").toString();
        	String expectInvest = jsonObject.get("expectInvest").toString();
        	String level = jsonObject.get("level").toString();
        	String des = jsonObject.get("des").toString();
        	
        	DataParam editParam = new DataParam
        			("OPP_ID",id,"OPP_NAME",name,"OPP_CONCERN_PRODUCT",id,"OPP_CONCERN_PRODUCT",productNames,
        					"OPP_EXPECT_INVEST",expectInvest,"OPP_STATE","0","OPP_LEVEL",level,
        					"OPP_DES",des,"OPP_CREATER",userId,"OPP_CREATE_TIME",new Date(),"CLUE_SALESMAN",userId);
        	
        	getService().editCustomersOppInfo(editParam);
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }

	@Override
	public String createCustomersOppInfo(String info) {
    	String responseText = "fail";
    	try {
        	JSONObject jsonObject = new JSONObject(info);
        	User user = (User) this.getUser();
        	String userId = user.getUserId();
        	
        	String id =  jsonObject.get("custId").toString();
        	String name = jsonObject.get("name").toString();
        	String productNames = jsonObject.get("productNames").toString();
        	String expectInvest = jsonObject.get("expectInvest").toString();
        	String level = jsonObject.get("level").toString();
        	String des = jsonObject.get("des").toString();
        	
        	DataParam createParam = new DataParam
        			("CUST_ID",id,"OPP_NAME",name,"OPP_CONCERN_PRODUCT",id,"OPP_CONCERN_PRODUCT",productNames,
        					"OPP_EXPECT_INVEST",expectInvest,"OPP_STATE","0","OPP_LEVEL",level,
        					"OPP_DES",des,"OPP_CREATER",userId,"OPP_CREATE_TIME",new Date(),"CLUE_SALESMAN",userId);
        	
        	getService().createCustomersOppInfo(createParam);
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }

	@Override
	public String deleteCustomersOppInfo(String id) {
    	String responseText = "fail";
    	try {
        	getService().deleteCustomersOppInfo(new DataParam("OPP_ID",id));
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }
	
	@Override
	public String updateOppStateInfo(String id, String state) {
		String responseText = "fail";
		DataParam param = new DataParam();
		param.put("OPP_ID", id);
		param.put("OPP_STATE", state);
		oppService().changeStateRecord(param);
		responseText = "success";
		return responseText;
	}

	@Override
	public String findCustomersContactsInfo(String custId) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			List<DataRow> reList = getService().findPersonRecords(new DataParam("CUST_ID",custId));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow.get("CONT_ID"));
				jsonObject1.put("name", dataRow.get("CONT_NAME"));
				jsonObject1.put("sex", dataRow.get("CONT_SEX"));
				jsonObject1.put("job", dataRow.get("CONT_JOB"));
				jsonObject1.put("phone", dataRow.get("CONT_PHONE"));
				jsonObject1.put("email", dataRow.get("CONT_EMAIL"));
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String getCustomersContInfo(String id) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			DataRow dataRow = getService().getCustomersContInfo(new DataParam("CONT_ID",id));
			
			jsonObject.put("id", dataRow.get("CONT_ID"));
			jsonObject.put("custId", dataRow.get("CUST_ID"));
			jsonObject.put("job", dataRow.get("CONT_JOB"));
			jsonObject.put("name", dataRow.get("CONT_NAME"));
			jsonObject.put("sex", dataRow.get("CONT_SEX"));
			jsonObject.put("phone", dataRow.get("CONT_PHONE"));
			jsonObject.put("email", dataRow.get("CONT_EMAIL"));
			jsonObject.put("other", dataRow.get("CONT_OTHER"));
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String createCustomersContactsInfo(String info) {
    	String responseText = "fail";
    	try {
        	JSONObject jsonObject = new JSONObject(info);
        	
        	String id = jsonObject.get("custId").toString();
        	String job = jsonObject.get("job").toString();
        	String name = jsonObject.get("name").toString();
        	String sex = jsonObject.get("sex").toString();
        	String phone = jsonObject.get("phone").toString();
        	String email = jsonObject.get("email").toString();
        	String other = jsonObject.get("other").toString();
        	
        	DataParam createParam = new DataParam
        			("CUST_ID",id,"CONT_JOB",job,"CONT_NAME",name,"CONT_SEX",sex,"CONT_PHONE",phone,"CONT_EMAIL",email,"CONT_OTHER",other);
        	
        	getService().createCustomersContactsInfo(createParam);
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }

	@Override
	public String editCustomersContactsInfo(String info) {
    	String responseText = "fail";
    	try {
        	JSONObject jsonObject = new JSONObject(info);
        	
        	String id = jsonObject.get("id").toString();
        	String job = jsonObject.get("job").toString();
        	String name = jsonObject.get("name").toString();
        	String sex = jsonObject.get("sex").toString();
        	String phone = jsonObject.get("phone").toString();
        	String email = jsonObject.get("email").toString();
        	String other = jsonObject.get("other").toString();
        	
        	DataParam editParam = new DataParam
        			("CONT_ID",id,"CONT_JOB",job,"CONT_NAME",name,"CONT_SEX",sex,"CONT_PHONE",phone,"CONT_EMAIL",email,"CONT_OTHER",other);
        	
        	getService().editCustomersContactsInfo(editParam);
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }

	@Override
	public String deleteCustomersContactsInfo(String id) {
    	String responseText = "fail";
    	try {
        	getService().deleteCustomersContactsInfo(new DataParam("CONT_ID",id));
        	responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return responseText;
    }

	@Override
	public String findCustomersOrderInfo(String custId) {
		String responseText = null;
		try {
			JSONObject jsonObject = new JSONObject();
			List<DataRow> reList = getService().findCustomersOrderInfo(new DataParam("CUST_ID",custId));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<reList.size();i++){
				DataRow dataRow = reList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("id", dataRow.get("ORDER_ID"));
				jsonObject1.put("name", dataRow.get("OPP_NAME"));
				jsonObject1.put("chief", dataRow.get("ORDER_CHIEF"));
				jsonObject1.put("salesmanName", dataRow.get("CLUE_SALESMAN_NAME"));
				jsonObject1.put("cost", dataRow.get("ORDER_COST")+"元");
				jsonObject1.put("datetime", dataRow.get("ORDER_CREATE_TIME"));
				String orderStateCode=dataRow.getString("ORDER_STATE");
				String orderState =FormSelectFactory.create("ORDER_STATE").getText(orderStateCode);
				jsonObject1.put("orderState", orderState);
				if(orderState.equals("提交")){
					jsonObject1.put("recordState", false);
				}else{
					jsonObject1.put("recordState", true);
				}
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("datas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String getCustomersOrderInfo(String orderId) {
		String responseText = null;
		try {
			DataParam param = new DataParam("ORDER_ID", orderId);
			DataRow dataRow = orderService().getMasterRecord(param);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", dataRow.get("ORDER_ID"));
			jsonObject.put("oppId", dataRow.get("OPP_ID"));
			jsonObject.put("name", dataRow.get("ORDER_NAME"));
			jsonObject.put("chief", dataRow.get("ORDER_CHIEF"));
			jsonObject.put("salesman", dataRow.get("CLUE_SALESMAN"));
			jsonObject.put("salesmanName", dataRow.get("CLUE_SALESMAN_NAME"));
			jsonObject.put("deliveryCost", dataRow.get("ORDER_DELIVERY_COST"));
			jsonObject.put("orderCost", dataRow.get("ORDER_COST"));
			jsonObject.put("state", dataRow.get("ORDER_STATE"));
			jsonObject.put("orderCreater", dataRow.get("ORDER_CREATER"));
			jsonObject.put("orderCreaterName", dataRow.get("ORDER_CREATER_NAME"));
			jsonObject.put("orderCreateTime", dataRow.get("ORDER_CREATE_TIME"));
			if(dataRow.get("ORDER_STATE").equals("1")){
				jsonObject.put("recordState", false );
			}else{
				jsonObject.put("recordState", true );
			}
			
			List<DataRow> orderEntryList = orderService().findSubRecords("OrderEntry", param);
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<orderEntryList.size();i++){
				DataRow orderEntryRow = orderEntryList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("entryId", orderEntryRow.get("ENTRY_ID"));
				jsonObject1.put("orderId", orderEntryRow.get("ORDER_ID"));
				jsonObject1.put("entryOrderProduct", orderEntryRow.get("ENTRY_ORDER_PRODUCT"));
				jsonObject1.put("entryProductModel", orderEntryRow.get("ENTRY_PRODUCT_MODEL"));
				jsonObject1.put("entryNumber", orderEntryRow.get("ENTRY_NUMBER"));
				jsonObject1.put("entryUnitPrice", orderEntryRow.get("ENTRY_UNIT_PRICE"));
				jsonObject1.put("entryDiscount", orderEntryRow.get("ENTRY_DISCOUNT"));
				jsonObject1.put("entryRealPrice", orderEntryRow.get("ENTRY_REAL_PRICE"));
				jsonArray.put(jsonObject1);
			}
			
			jsonObject.put("entryDatas", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String getCustomersOrderEntryInfo(String id) {
		String responseText = null;
		try {
			DataRow dataRow = orderService().getSubRecord("OrderEntry", new DataParam("ENTRY_ID", id));
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("entryId", dataRow.get("ENTRY_ID"));
			jsonObject.put("orderId", dataRow.get("ORDER_ID"));
			jsonObject.put("entryOrderProduct", dataRow.get("ENTRY_ORDER_PRODUCT"));
			jsonObject.put("entryProductModel", dataRow.get("ENTRY_PRODUCT_MODEL"));
			jsonObject.put("entryNumber", dataRow.get("ENTRY_NUMBER"));
			jsonObject.put("entryUnitPrice", dataRow.get("ENTRY_UNIT_PRICE"));
			jsonObject.put("entryDiscount", dataRow.get("ENTRY_DISCOUNT"));
			jsonObject.put("entryRealPrice", dataRow.get("ENTRY_REAL_PRICE"));
			
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String createCustomersOrderInfo(String info) {
		String responseText = "fail";
		try {
        	JSONObject jsonObject = new JSONObject(info);
        	
        	String id = KeyGenerator.instance().genKey();
        	String oppId = jsonObject.get("oppId").toString();
        	String chief = jsonObject.get("chief").toString();
        	String deliveryCost = jsonObject.get("deliveryCost").toString();
        	String orderCost = jsonObject.get("orderCost").toString();
        	User user = (User) getUser();
        	String orderCreater = user.getUserId();
        	String dateTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
        	String orderCreateTime = dateTime;
        	String saleMan = jsonObject.get("saleMan").toString();
        	
			DataParam param = new DataParam("ORDER_ID",id,"OPP_ID",oppId,
					"ORDER_CHIEF",chief,"ORDER_DELIVERY_COST",deliveryCost,
					"ORDER_COST",orderCost,"ORDER_STATE",0,
					"ORDER_CREATER",orderCreater,"ORDER_CREATE_TIME",orderCreateTime,"CLUE_SALESMAN",saleMan);
			orderService().createMasterRecord(param);
			responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String createCustomersOrderEntryInfo(String info) {
		String responseText = "fail";
		try {
			JSONObject jsonObject = new JSONObject(info);
			String entryId = KeyGenerator.instance().genKey();
			String orderId = jsonObject.getString("orderId");
			String entryOrderProduct = jsonObject.getString("entryOrderProduct");
			String entryProductModel = jsonObject.getString("entryProductModel");
			String entryNumber = jsonObject.getString("entryNumber");
			String entryUnitPrice = jsonObject.getString("entryUnitPrice");
			String entryDiscount = jsonObject.getString("entryDiscount");
			String entryRealPrice = jsonObject.getString("entryRealPrice");
			
			DataParam param = new DataParam("ENTRY_ID",entryId,"ORDER_ID",orderId,
					"ENTRY_ORDER_PRODUCT",entryOrderProduct,"ENTRY_PRODUCT_MODEL",entryProductModel,
					"ENTRY_NUMBER",entryNumber,"ENTRY_UNIT_PRICE",entryUnitPrice,
					"ENTRY_DISCOUNT",entryDiscount,"ENTRY_REAL_PRICE",entryRealPrice);
			
			orderService().createSubRecord("OrderEntry", param);
			
			responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		
		return responseText;
	}
	
	@Override
	public String updateCustomersOrderInfo(String info) {
		String responseText = "fail";
		try {
        	JSONObject jsonObject = new JSONObject(info);
        	
        	String id = jsonObject.get("id").toString();
        	String oppId = jsonObject.get("oppId").toString();
        	String chief = jsonObject.get("chief").toString();
        	String deliveryCost = jsonObject.get("deliveryCost").toString();
        	String orderCost = jsonObject.get("orderCost").toString();
        	String state = jsonObject.get("state").toString();
        	User user = (User) getUser();
        	String orderCreater = user.getUserId();
        	String dateTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
        	String orderCreateTime = dateTime;
        	String saleMan = jsonObject.get("salesman").toString();
        	
			DataParam param = new DataParam("ORDER_ID",id,"OPP_ID",oppId,
					"ORDER_CHIEF",chief,"ORDER_DELIVERY_COST",deliveryCost,
					"ORDER_COST",orderCost,"ORDER_STATE",state,
					"ORDER_CREATER",orderCreater,"ORDER_CREATE_TIME",orderCreateTime,"CLUE_SALESMAN",saleMan);
			orderService().updateMasterRecord(param);
			responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String updateCustomersOrderEntryInfo(String info) {
		String responseText = "fail";
		try {
			JSONObject jsonObject = new JSONObject(info);
			String entryId = jsonObject.getString("entryId");
			String orderId = jsonObject.getString("orderId");
			String entryOrderProduct = jsonObject.getString("entryOrderProduct");
			String entryProductModel = jsonObject.getString("entryProductModel");
			String entryNumber = jsonObject.getString("entryNumber");
			String entryUnitPrice = jsonObject.getString("entryUnitPrice");
			String entryDiscount = jsonObject.getString("entryDiscount");
			String entryRealPrice = jsonObject.getString("entryRealPrice");
			
			DataParam param = new DataParam("ENTRY_ID",entryId,"ORDER_ID",orderId,
					"ENTRY_ORDER_PRODUCT",entryOrderProduct,"ENTRY_PRODUCT_MODEL",entryProductModel,
					"ENTRY_NUMBER",entryNumber,"ENTRY_UNIT_PRICE",entryUnitPrice,
					"ENTRY_DISCOUNT",entryDiscount,"ENTRY_REAL_PRICE",entryRealPrice);
			
			orderService().updateSubRecord("OrderEntry", param);
			
			responseText = "success";
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		
		return responseText;
	}

	@Override
	public String updateOrderStateInfo(String id, String state) {
		String responseText = "fail";
		DataParam param = new DataParam();
		param.put("ORDER_ID", id);
		param.put("ORDER_STATE", state);
		User user = (User) getUser();
		param.put("ORDER_CONFIRM_PERSON", user.getUserId());
		String confirmTime = DateUtil.getDateByType(DateUtil.YYMMDDHHMI_HORIZONTAL, new Date());
		param.put("ORDER_CONFIRM_TIME", confirmTime);
		orderService().changeStateRecord(param);
		responseText = "success";
		return responseText;
	}

	@Override
	public String deleteCustomersOrderInfo(String id) {
		String responseText = "fail";
		orderService().deleteClusterRecords(new DataParam("ORDER_ID", id));
		responseText = "success";
		return responseText;
	}
	
	@Override
	public String deleteCustomersOrderEntryInfo(String id) {
		String responseText = "fail";
		orderService().deleteSubRecord("OrderEntry", new DataParam("ENTRY_ID", id));
		responseText = "success";
		return responseText;
	}
	
	@Override
	public String findSelect() {
		String responseText = null;
		try {
	    	User user = (User) getUser();
	    	PrivilegeHelper privilegeHelper = new PrivilegeHelper(user);
	    	DataParam saleManParam = new DataParam();
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
	    	if(!privilegeHelper.isSalesDirector()){
	    		saleManParam.put("userId",user.getUserId());
	    		JSONObject jsonObject1 = new JSONObject();
	    		jsonObject1.put("level", "");
	    		jsonObject1.put("progressState", "");
	    		jsonObject1.put("saleMan", user.getUserId());
	    		jsonObject.put("search", jsonObject1);
	    	}else{
	    		JSONObject jsonObject1 = new JSONObject();
	    		jsonObject1.put("value", "");
	    		jsonObject1.put("name", "全部销售");
	    		jsonArray.put(jsonObject1);
	    		jsonObject.put("saleManDatas", jsonArray);
	    	}
	    	List<DataRow> salMans = getService().findSaleManRecords(saleManParam);
			for(int i=0;i<salMans.size();i++){
				DataRow dataRow = salMans.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("value", dataRow.get("USER_ID"));
				jsonObject1.put("name", dataRow.get("USER_NAME"));
				jsonArray.put(jsonObject1);
			}
			jsonObject.put("saleManDatas", jsonArray);
			
			DataParam progressStateParam = new DataParam("TYPE_ID","CUST_PROGRESS_STATE");
			List<DataRow> progressStateList = getService().findProgressStateRecords(progressStateParam);
			JSONArray jsonArray1 = new JSONArray();
			for(int i=0;i<progressStateList.size();i++){
				DataRow dataRow = progressStateList.get(i);
				JSONObject jsonObject1 = new JSONObject();
				jsonObject1.put("value", dataRow.get("CODE_ID"));
				jsonObject1.put("name", dataRow.get("CODE_NAME"));
				jsonArray1.put(jsonObject1);
			}
			jsonObject.put("progressState", jsonArray1);
			
			DataParam levelParam = new DataParam("TYPE_ID","CUST_LEVEL");
			List<DataRow> levelList = getService().findProgressStateRecords(levelParam);
			JSONArray jsonArray2 = new JSONArray();
			for(int i=0;i<levelList.size();i++){
				DataRow dataRow = levelList.get(i);
				JSONObject jsonObject2 = new JSONObject();
				jsonObject2.put("value", dataRow.get("CODE_ID"));
				jsonObject2.put("name", dataRow.get("CODE_NAME"));
				jsonArray2.put(jsonObject2);
			}
			jsonObject.put("level", jsonArray2);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
	
	@Override
	public String findContList(String custId) {
		String rspText = null;
		try {
			List<DataRow> rsList =  contListSelectService().queryPickFillRecords(new DataParam("custId",custId));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				JSONObject jsonObeject = new JSONObject();
				DataRow dataRow = rsList.get(i);
				jsonObeject.put("contId",dataRow.get("CONT_ID"));
				jsonObeject.put("contName",dataRow.get("CONT_NAME"));
				jsonObeject.put("contJob",dataRow.get("CONT_JOB"));
				jsonObeject.put("custName",dataRow.get("CUST_NAME"));
				jsonArray.put(jsonObeject);
			}
			rspText = jsonArray.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return rspText;
	}
	
	@Override
	public String findHomeCardRecords() {
		String rspText = null;
		try {
			String weekBegin = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getBeginOfWeek(new Date()))+" 00:00:00";
			String weekEnd =DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getBeginOfWeek(new Date()), DateUtil.DAY, 6))+" 23:59:59";
			List<DataRow> rsList = getService().findOppOrderWeekRecords(new DataParam("weekBegin", weekBegin,"weekEnd", weekEnd));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				JSONObject jsonObject = new JSONObject();
				DataRow dataRow = rsList.get(i);
				jsonObject.put("custName", dataRow.get("CUST_NAME"));
				jsonObject.put("oppNum", dataRow.get("OPP_NUM"));
				jsonObject.put("orderNum", dataRow.get("ORDER_NUM"));
				jsonArray.put(jsonObject);
			}
			
			rspText = jsonArray.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return rspText;
	}
	
	@Override
	public String findOppOrderRecords(String saleId) {
		String rspText = null;
		try {
			String weekBegin = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getBeginOfWeek(new Date()))+" 00:00:00";
			String weekEnd =DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(DateUtil.getBeginOfWeek(new Date()), DateUtil.DAY, 6))+" 23:59:59";
			List<DataRow> rsList = getService().findOppOrderRecords(new DataParam("weekBegin", weekBegin,"weekEnd", weekEnd, "saleId", saleId));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				JSONObject jsonObject = new JSONObject();
				DataRow dataRow = rsList.get(i);
				jsonObject.put("custId", dataRow.get("CUST_ID"));
				jsonObject.put("custName", dataRow.get("CUST_NAME"));
				jsonObject.put("oppNum", dataRow.get("OPP_NUM"));
				jsonObject.put("orderNum", dataRow.get("ORDER_NUM"));
				jsonArray.put(jsonObject);
			}
			
			rspText = jsonArray.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return rspText;
	}
	
	public String findHomeCardSaleListRecords() {
		String rspText = null;
		try {
			SalerListSelect service = this.lookupService(SalerListSelect.class);
			List<DataRow> rsList = service.querySaleListRecords();
			JSONObject jsonObject = new JSONObject();
			JSONArray jsonArrayUserInfos = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				DataRow dataRow = rsList.get(i);
				JSONObject jsonObjectUserInfos = new JSONObject();
				jsonObjectUserInfos.put("userId", dataRow.getString("USER_ID"));
				jsonObjectUserInfos.put("userName", dataRow.getString("USER_NAME"));
				jsonObjectUserInfos.put("userCode", dataRow.getString("USER_CODE"));
				jsonArrayUserInfos.put(jsonObjectUserInfos);
			}
			JSONArray jsonArrayUserCodes = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				DataRow dataRow = rsList.get(i);
				jsonArrayUserCodes.put(dataRow.getString("USER_CODE"));
			}
			jsonObject.put("userInfos", jsonArrayUserInfos);
			jsonObject.put("userCodes", jsonArrayUserCodes);
			rspText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return rspText;
	}
	
	protected CustomerGroup8ContentManage getService() {
		return (CustomerGroup8ContentManage) this.lookupService(CustomerGroup8ContentManage.class);
	}
	
	protected VisitManage visitService() {
		return (VisitManage) this.lookupService(VisitManage.class);
	}
	
    protected OppInfoManage oppService() {
        return (OppInfoManage) this.lookupService(OppInfoManage.class);
    }
    
    protected OrderInfoManage orderService() {
        return (OrderInfoManage) this.lookupService(OrderInfoManage.class);
    }
    
    protected ContListSelect contListSelectService() {
        return (ContListSelect) this.lookupService(ContListSelect.class);
    }
}
