package com.agileai.crm.service.common;

import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.crm.cxmodule.WcmGeneralGroup8ContentManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.ws.BaseRestService;


public class FormSelectUtilImpl extends BaseRestService implements FormSelectUtil {

	@Override
	public String findCodeList(String codeType) {
		String responseText = "";
		try {

			WcmGeneralGroup8ContentManage service = this.lookupService(WcmGeneralGroup8ContentManage.class);
			List<DataRow> rsList = service.findCodeListRecords(new DataParam("TYPE_ID", codeType));
			JSONArray jsonArray = new JSONArray();
			for(int i=0;i<rsList.size();i++){
				DataRow dataRow = rsList.get(i);
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("codeId", dataRow.get("CODE_ID"));
				jsonObject.put("codeName", dataRow.get("CODE_NAME"));
				jsonArray.put(jsonObject);
			}
			responseText = jsonArray.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}
}
