angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit,$ionicActionSheet,$timeout,$stateParams){
	//搜索
	$scope.searchCustomerName = function(){
		if($scope.searchWord == undefined){
			$scope.searchWord = "";
		}
		
		$scope.infoParam='{"taskReviewId":"'+$stateParams.taskReviewId+'","name":"'+$scope.searchWord+'"}';
		if("StrangeVisit" == $scope.activeTab){//陌生拜访创建记录
			$scope.loadPotentialCustomerListData = function(){
				var url = '/aeaicrm/services/MyTasks/rest/potential-cust-list/'+$scope.infoParam;
				AppKit.getJsonApi(url).success(function(rspJson){
					$scope.strangeCustomerList = rspJson.datas;
				});	
			}
			$scope.loadPotentialCustomerListData();
		}else if("IntentionFollowUp" == $scope.activeTab){//意向跟进创建记录
			$scope.loadMyCustomerListData = function(){
				var url = '/aeaicrm/services/MyTasks/rest/my-cust-list/'+$scope.infoParam;
				AppKit.getJsonApi(url).success(function(rspJson){
					$scope.intentionCustomerList = rspJson.datas;
				});	
			}
			$scope.loadMyCustomerListData();
		}
	}
	
});