angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit,$ionicActionSheet,$timeout,$stateParams,$state){
		
		$scope.customerList = function(){
			$scope.searchWord={"level":$scope.level,"progressState":$scope.progressState,"saleMan":""};
			var url = '/aeaicrm/services/Customers/rest/list/'+JSON.stringify($scope.searchWord);
			AppKit.getJsonApi(url).success(function(rspJson){
					$scope.listInfo=rspJson.datas;
			});
		};
		$scope.customerList();
		
		$scope.openModal = function(id,type) {
			if(type=="details"){
				$state.go('tab.my-customer-information', {"id": id});
			}
		}; 
		
		$scope.deleteCustomer = function(id){
			AppKit.confirm({operaType:'delete',action:function(){
				var url = "/aeaicrm/services/Customers/rest/delete-info/"+id;
				AppKit.getJsonApi(url).success(function(rspJson){
					$scope.customerList();
				});
			}});
		};
});